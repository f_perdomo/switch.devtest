﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummaryOne()
        {
            // Arrange
            Bank bank = new Bank();
            Customer john = new Customer("John");

            // Act
            bank.AddCustomer(john);
            bank.OpenAccount(john, AccountType.CHECKING);

            // Assert
            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CustomerSummaryMultiple()
        {
            Bank bank = new Bank();
            Customer anttuan = new Customer("Anttuan");
            bank.AddCustomer(anttuan);

            bank.OpenAccount(anttuan, AccountType.CHECKING);
            bank.OpenAccount(anttuan, AccountType.SAVINGS);

            Assert.AreEqual("Customer Summary\n - Anttuan (2 accounts)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");

            bank.AddCustomer(bill);
            Account checkingAccount = bank.OpenAccount(bill, AccountType.CHECKING);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            bank.AddCustomer(bill);

            Account checkingAccount = bank.OpenAccount(bill, AccountType.SAVINGS);

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            bank.AddCustomer(bill);

            Account checkingAccount = bank.OpenAccount(bill, AccountType.MAXI_SAVINGS);

            checkingAccount.Deposit(3000.0);

            Assert.AreEqual(150.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

    }
}
