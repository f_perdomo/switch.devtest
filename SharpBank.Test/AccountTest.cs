﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void TestDeposit()
        {
            // Arrange
            Bank bank = new Bank();
            Customer lil = new Customer("Lil");
            bank.AddCustomer(lil);

            // Act
            Account accChecking = bank.OpenAccount(lil, AccountType.CHECKING);
            accChecking.Deposit(5000.0);

            // Assert
            Assert.AreEqual(5000.0, accChecking.ActualAmount());
        }

        [Test]
        public void TestDepositLessZero()
        {
            Bank bank = new Bank();
            Customer ruben = new Customer("ruben");
            bank.AddCustomer(ruben);

            Account accChecking = bank.OpenAccount(ruben, AccountType.CHECKING);

            var ex = Assert.Throws<ArgumentException>(() => accChecking.Deposit(-100.0));
            Assert.That(ex.Message, Is.EqualTo("amount must be greater than zero"));
        }

        [Test]
        public void TestWithdraw()
        {
            Bank bank = new Bank();
            Customer lil = new Customer("Lil");
            bank.AddCustomer(lil);

            Account accChecking = bank.OpenAccount(lil, AccountType.CHECKING);
            accChecking.Deposit(5000.0);
            accChecking.Withdraw(4000.0);

            Assert.AreEqual(1000.0, accChecking.ActualAmount());
        }

        [Test]
        public void TestWithdrawZero()
        {
            Bank bank = new Bank();
            Customer lewis = new Customer("Lewis");
            bank.AddCustomer(lewis);

            Account accChecking = bank.OpenAccount(lewis, AccountType.CHECKING);

            var ex = Assert.Throws<ArgumentException>(() => accChecking.Withdraw(0));
            Assert.That(ex.Message, Is.EqualTo("amount must be greater than zero"));
        }

        [Test]
        public void TestInterestEarnedBasic()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            bank.AddCustomer(bill);

            Account checkingAccount = bank.OpenAccount(bill, AccountType.SAVINGS);

            checkingAccount.Deposit(3000.0);
            Assert.AreEqual(5.0, checkingAccount.InterestEarned());
        }

        [Test]
        public void TestInterestEarnedFull()
        {
            Bank bank = new Bank();
            Customer jack = new Customer("Jack");
            bank.AddCustomer(jack);

            Account accChecking = bank.OpenAccount(jack, AccountType.CHECKING);
            Account accSavings = bank.OpenAccount(jack, AccountType.SAVINGS);
            Account accMaxSavings = bank.OpenAccount(jack, AccountType.MAXI_SAVINGS);

            accChecking.Deposit(10000.0);
            accSavings.Deposit(1000.0);
            accMaxSavings.Deposit(1000.0);

            double total = accChecking.InterestEarned();
            Assert.AreEqual(total, accChecking.ActualAmount() * 0.001);

            total = accMaxSavings.InterestEarned();
            Assert.AreEqual(total, accMaxSavings.ActualAmount() * 0.05);

            accMaxSavings.Withdraw(100.0);
            total = accMaxSavings.InterestEarned();
            Assert.AreEqual(total, accMaxSavings.ActualAmount() * 0.001);
        }

        [Test]
        public void TestInterestEarnedSaving()
        {
            Bank bank = new Bank();
            Customer rose = new Customer("Rose");
            bank.AddCustomer(rose);

            Account accSavings = bank.OpenAccount(rose, AccountType.SAVINGS);

            accSavings.Deposit(700.0);

            double total = accSavings.InterestEarned();
            Assert.AreEqual(total, accSavings.ActualAmount() * 0.001);
        }
        
        [Test]
        public void TestTransferSameAccount()
        {
            Bank bank = new Bank();
            Customer jorge = new Customer("jorge");
            bank.AddCustomer(jorge);

            Account accChecking = bank.OpenAccount(jorge, AccountType.CHECKING);
            accChecking.Deposit(5000.0);

            var ex = Assert.Throws<ArgumentException>(() => accChecking.Transfer(2500.0, accChecking));
            Assert.That(ex.Message, Is.EqualTo("you can't transfer money to the same account"));
        }

        [Test]
        public void TestTransferDifferentAccount()
        {
            Bank bank = new Bank();
            Customer juls = new Customer("juls");
            bank.AddCustomer(juls);

            Account accChecking = bank.OpenAccount(juls, AccountType.CHECKING);
            Account accSaving = bank.OpenAccount(juls, AccountType.SAVINGS);

            accChecking.Deposit(5000.0);
            accChecking.Transfer(3000.0, accSaving);

            Assert.AreEqual(2000.0, accChecking.ActualAmount());
            Assert.AreEqual(3000.0, accSaving.ActualAmount());
        }

        [Test]
        public void TestInvalidTransfer()
        {
            Bank bank = new Bank();
            Customer nico = new Customer("Nico");
            bank.AddCustomer(nico);

            Account accChecking = bank.OpenAccount(nico, AccountType.CHECKING);
            Account accSaving = bank.OpenAccount(nico, AccountType.SAVINGS);

            accChecking.Deposit(1000.0);

            var ex = Assert.Throws<ArgumentException>(() => accChecking.Transfer(3000.0, accSaving));
            Assert.That(ex.Message, Is.EqualTo("Error in transfer, because: the account not have enough money"));
        }
    }
}
