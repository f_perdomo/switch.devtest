﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        [Test]
        public void TestCustomerStatementGeneration()
        {
            // Arrange
            Bank bank = new Bank();
            Customer henry = new Customer("Henry");
            bank.AddCustomer(henry);

            Account checkingAccount = bank.OpenAccount(henry, AccountType.CHECKING);
            Account savingsAccount = bank.OpenAccount(henry, AccountType.SAVINGS);

            // Act
            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            // Assert
            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Bank bank = new Bank();
            Customer oscar = new Customer("Oscar");
            bank.AddCustomer(oscar);

            Account savingAccount = bank.OpenAccount(oscar, AccountType.SAVINGS);

            Assert.AreEqual(1, oscar.AccountsNumber());
        }

        [Test]
        public void TestTwoAccount()
        {
            Bank bank = new Bank();
            Customer oscar = new Customer("Oscar");
            bank.AddCustomer(oscar);

            Account savingAccount = bank.OpenAccount(oscar, AccountType.SAVINGS);
            Account checkingAccount = bank.OpenAccount(oscar, AccountType.CHECKING);

            Assert.AreEqual(2, oscar.AccountsNumber());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Bank bank = new Bank();
            Customer oscar = new Customer("Oscar");
            bank.AddCustomer(oscar);

            Account savingAccount = bank.OpenAccount(oscar, AccountType.SAVINGS);
            Account checkingAccount = bank.OpenAccount(oscar, AccountType.CHECKING);
            Account maxSavingAccount = bank.OpenAccount(oscar, AccountType.MAXI_SAVINGS);

            Assert.AreEqual(3, oscar.AccountsNumber());
        }
    }
}
