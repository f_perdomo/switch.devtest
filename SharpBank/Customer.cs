﻿using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private string name;
        private List<Account> accounts;

        public Customer(string name)
        {
            this.name = name;
            accounts = new List<Account>();
        }

        public string GetName()
        {
            return name;
        }

        public void AddAccount(Account account)
        {
            accounts.Add(account);
        }

        public int AccountsNumber()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        public string GetStatement()
        {
            string statement = null;
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + a.StatementForAccount() + "\n";
                total += a.ActualAmount();
            }
            statement += "\nTotal In All Accounts " + Utils.ToDollars(total);
            return statement;
        }
    }
}
