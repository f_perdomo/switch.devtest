﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        private readonly double amount;
        private DateTime transactionDate;
        private string typeText;
        private TransactionType type;

        public double Amount()
        {
            return amount;
        }

        public string TypeText()
        {
            return typeText;
        }
        public Transaction(double amount)
        {
            this.amount = amount;
            transactionDate = DateProvider.Instance.Now();
            type = amount < 0 ? TransactionType.WITHDRAWAL : TransactionType.DEPOSIT;
            typeText = type == TransactionType.WITHDRAWAL ? "withdrawal" : "deposit";
        }

    }
}
