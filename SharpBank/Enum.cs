﻿namespace SharpBank
{
    public enum AccountType : short
    {
        CHECKING = 0,
        SAVINGS = 1,
        MAXI_SAVINGS = 2
    }

    public enum TransactionType : short
    {
        WITHDRAWAL = 0,
        DEPOSIT = 1
    }
}
