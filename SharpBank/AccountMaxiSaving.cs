﻿namespace SharpBank
{
    public class AccountMaxiSaving : Account
    {
        public AccountMaxiSaving(long accountNumber) :
            base(accountNumber, AccountType.MAXI_SAVINGS)
        { }

        public override double InterestEarned()
        {
            double percentage = 0.1;
            int interestDays = (int)(DateProvider.Instance.Now() - accountDateCreate).TotalDays;
            double amount = 0.0;

            if (!lastWithdraw.HasValue || (DateProvider.Instance.Now() - lastWithdraw.Value).TotalDays > 10)
            {
                percentage = 0.5;
                amount = actualAmount + InterestGenerated(actualAmount, percentage, interestDays);
                return amount * 0.05;
            }
            amount = actualAmount + InterestGenerated(actualAmount, percentage, interestDays);
            return actualAmount * 0.001;
        }
    }
}
