﻿using System;

namespace SharpBank
{
    public class DateProvider
    {
        private readonly static DateProvider _instance = new DateProvider();

        private DateProvider()
        {
        }

        public static DateProvider Instance
        {
            get
            {
                return _instance;
            }
        }

        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}
