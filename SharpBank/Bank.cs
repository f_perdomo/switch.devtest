﻿using System.Collections.Generic;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;
        private long totalAccounts;

        public Bank()
        {
            customers = new List<Customer>();
            totalAccounts = 0;
        }

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public Account OpenAccount(Customer customer, AccountType accountType)
        {
            totalAccounts += 1;
            Account account = null;
            switch (accountType)
            {
                case AccountType.SAVINGS:
                    account = new AccountSaving(totalAccounts);
                    break;
                case AccountType.MAXI_SAVINGS:
                    account = new AccountMaxiSaving(totalAccounts);
                    break;
                default: //AccountTypeEnum.CHECKING:
                    account = new AccountChecking(totalAccounts);
                    break;
            }

            customer.AddAccount(account);
            return account;
        }

        public string CustomerSummary()
        {
            string summary = "Customer Summary";
            foreach (Customer c in customers)
                summary += "\n - " + c.GetName() + " (" + Utils.AccountFormat(c.AccountsNumber(), "account") + ")";
            return summary;
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            foreach (Customer c in customers)
                total += c.TotalInterestEarned();
            return total;
        }
    }
}
