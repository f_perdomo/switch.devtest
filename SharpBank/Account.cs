﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public abstract class Account
    {
        private long accountNumber;
        private readonly AccountType accountType;
        private string accountTypeText;
        private List<Transaction> transactions;

        protected double actualAmount;
        protected DateTime? lastWithdraw;
        protected DateTime accountDateCreate;

        public Account(long accountNumber, AccountType accountType)
        {
            this.accountNumber = accountNumber;
            this.accountType = accountType;
            accountTypeText = AccountTypeText(accountType);
            transactions = new List<Transaction>();
            actualAmount = 0.0;
            lastWithdraw = null;
            accountDateCreate = DateProvider.Instance.Now();
        }

        #region Properties
        public long AccountNumber()
        {
            return accountNumber;
        }
        public double ActualAmount()
        {
            return actualAmount;
        }
        public string StatementForAccount()
        {
            double total = 0.0;
            string statementResult = accountTypeText;

            foreach (Transaction t in transactions)
            {
                statementResult += string.Format("  {0} {1}\n", t.TypeText(), Utils.ToDollars(t.Amount()));
                total += t.Amount();
            }
            statementResult += string.Format("Total {0}", Utils.ToDollars(total));
            return statementResult;
        }
        #endregion

        private string AccountTypeText(AccountType accountType)
        {
            string accTypeText = string.Empty;
            switch (accountType)
            {
                case AccountType.CHECKING:
                    accTypeText = "Checking Account\n";
                    break;
                case AccountType.SAVINGS:
                    accTypeText = "Savings Account\n";
                    break;
                case AccountType.MAXI_SAVINGS:
                    accTypeText = "Maxi Savings Account\n";
                    break;
            }
            return accTypeText;
        }

        protected double InterestGenerated(double amount, double percentage, int days)
        {
            int totalDays = 365;
            double rate = percentage / 100.0;
            return amount * (rate / totalDays) * days;
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                actualAmount += amount;
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else if (amount > actualAmount)
            {
                throw new ArgumentException("the account not have enough money");
            }
            else
            {
                actualAmount -= amount;
                transactions.Add(new Transaction(-amount));
                lastWithdraw = DateProvider.Instance.Now();
            }
        }      

        public void Transfer(double amount, Account destinationAcc)
        {
            if (destinationAcc != null)
            {
                if (accountNumber == destinationAcc.AccountNumber())
                    throw new ArgumentException("you can't transfer money to the same account");
                else
                {
                    try
                    {
                        Withdraw(amount);
                        destinationAcc.Deposit(amount);
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException(
                            string.Format("Error in transfer, because: {0}", ex.Message));
                    }
                }
            }
        }

        public abstract double InterestEarned();
    }
}
