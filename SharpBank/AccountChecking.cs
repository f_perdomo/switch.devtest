﻿namespace SharpBank
{
    public class AccountChecking : Account
    {
        public AccountChecking(long accountNumber) :
            base(accountNumber, AccountType.CHECKING)
        { }

        public override double InterestEarned()
        {
            int interestDays = (int)(DateProvider.Instance.Now() - base.accountDateCreate).TotalDays;
            double percentage = 0.1;
            double amount = base.actualAmount + base.InterestGenerated(actualAmount, percentage, interestDays);
            return amount * 0.001;
        }
    }
}
