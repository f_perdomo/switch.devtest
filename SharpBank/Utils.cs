﻿using System;

namespace SharpBank
{
    public static class Utils
    {
        public static string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        public static string AccountFormat(int number, string word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }
    }
}
