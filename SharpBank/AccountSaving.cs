﻿namespace SharpBank
{
    public class AccountSaving : Account
    {
        public AccountSaving(long accountNumber) :
            base(accountNumber, AccountType.SAVINGS)
        { }

        public override double InterestEarned()
        {
            double percentage = 0.0;
            int interestDays = (int)(DateProvider.Instance.Now() - accountDateCreate).TotalDays;
            double amount = 0.0;

            if (actualAmount <= 1000)
            {
                percentage = 0.1;
                amount = actualAmount + InterestGenerated(actualAmount, percentage, interestDays);
                amount = amount * 0.001;
            }
            else
            {
                //The 0.1% for the first $1,000
                var amountOneThousand = 1000;
                percentage = 0.1;
                var interestOneThousand = InterestGenerated(amountOneThousand, percentage, interestDays);
                interestOneThousand = amountOneThousand * 0.001 + interestOneThousand;

                //The rest 0.2%
                percentage = 0.2;
                var interestRest = InterestGenerated(actualAmount - amountOneThousand, percentage, interestDays);

                amount = (actualAmount - amountOneThousand) * 0.002 + interestOneThousand + interestRest;
            }

            return amount;
        }
    }
}
